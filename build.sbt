lazy val root = ScavengerBuild.root

lazy val core = ScavengerBuild.core
lazy val prover = ScavengerBuild.prover
lazy val commandLine = ScavengerBuild.commandLine
