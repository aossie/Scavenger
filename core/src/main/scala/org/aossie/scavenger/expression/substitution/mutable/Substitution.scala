package org.aossie.scavenger.expression
package substitution
package mutable

import collection.mutable.{Map => MMap}
import scala.collection.MapFactory.toBuildFrom
import scala.collection.{BuildFrom, mutable}

final class Substitution (private val mm: MMap[Var, E])
extends AbstractSubstitution with MMap[Var, E] {
  override protected def m = mm.toMap

  def toImmutable = new immutable.Substitution(mm.toMap)

  def get(key: Var) = mm.get(key)
  override def update(key: Var, e: E) = mm.update(key, e)
  override def remove(key: Var): Option[E] = mm.remove(key)
  def iterator: Iterator[(Var, E)] = mm.iterator
  override def addOne(elem: (Var, E)): this.type = { update(elem._1, elem._2); this }
  override def subtractOne(elem: Var): this.type = { remove(elem); this }
  override def empty = new Substitution(MMap())
  override def stringPrefix = "Substitution"
}
object Substitution extends {
  def empty = new Substitution(MMap())

  def apply(kvs: (Var, E)*): Substitution = { val s = empty; for (kv <- kvs) s += kv ; s }

  implicit def canBuildFrom: BuildFrom[Substitution, (Var, E), Substitution] =
      new BuildFrom[Substitution, (Var, E), Substitution] {
        override def fromSpecific(from: Substitution)(it: IterableOnce[(Var, E)]): Substitution =
          new Substitution(MMap.fromSpecific(from)(it))

        override def newBuilder(from: Substitution): mutable.Builder[(Var, E), Substitution] =
          MMap.newBuilder[Var, E].mapResult(new Substitution(_))
      }
}
