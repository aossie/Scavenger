package org.aossie.scavenger.expression
package substitution
package immutable

import scala.collection.MapFactory.toBuildFrom
import scala.collection.{BuildFrom, mutable}

final class Substitution (override protected val m: Map[Var, E])
extends AbstractSubstitution with Map[Var, E] {
  def get(key: Var) = m.get(key)
  def iterator: Iterator[(Var, E)] = m.iterator
  override def updated[V1 >: E](key: Var, value: V1): Map[Var, V1] =
    value match {
      case e: E => new Substitution(m.updated(key, e))
      case _ => m.updated(key, value)
    }
  override def removed(key: Var): Map[Var, E] = new Substitution(m - key)
  override def empty = new Substitution(Map[Var,E]())
  override def stringPrefix = "Substitution"

  def apply(other: Substitution): Substitution = {
    val composedSubstitution = for ((key, value) <- this) yield (key, other(value))
    val left = other.filterNot { case (k, v) => this.contains(k) }
    new Substitution(composedSubstitution ++ left)
  }
}
object Substitution {
  def empty = new Substitution(Map[Var,E]())
  def apply(kvs: (Var, E)*): Substitution = new Substitution(Map[Var,E](kvs:_*))

  implicit def canBuildFrom: BuildFrom[Substitution, (Var, E), Substitution] =
      new BuildFrom[Substitution, (Var, E), Substitution] {
        override def fromSpecific(from: Substitution)(it: IterableOnce[(Var, E)]): Substitution =
          new Substitution(Map.fromSpecific(from)(it))

        override def newBuilder(from: Substitution): mutable.Builder[(Var, E), Substitution] =
          Map.newBuilder[Var, E].mapResult(new Substitution(_))
      }
}
